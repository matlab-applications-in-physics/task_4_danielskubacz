%MATLAB 2020b
%Name: bias
%Author: Daniel Skubacz
%Date: 02.01.2021

%finding the most common value - bias value
function [bias_level] = bias(compartments, all_counts)
    maximum_count = 0;
    
    for value = 1:200
        if maximum_count < all_counts(value)
            bias_level = value
            maximum_count = all_counts(value)
            
        end
    end
end
