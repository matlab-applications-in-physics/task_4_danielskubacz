%MATLAB 2020b
%Name: pulses_count
%Author: Daniel Skubacz
%Date: 02.01.2021

function [pulses_count] = countPulses(file_name, all_counts, bias_level)
    file_name = 'Co60_1350V_x20_p7_sub';
    file = fopen(strcat(file_name,'.dat'));
    i = 0;
    
    %choosing the size of the part of file:
    size = 1000000;
    
    %creating variables:
    all_counts = 0;
    pulses_count = 0;
    
    %collecting data to the histogram from whole file:
    while ~feof(file)
        fseek(file, size*i, 'bof');
        data = fread(file, size, 'uint8');
        
        [counts, compartments] = histcounts(data, 'BinLimits',[0.5, 200.5], 'BinWidth', 1);
        all_counts = all_counts + counts;
        i = i + 1;
        
        %variable for average value:
        average = 0;
        acceptable_average_max = 25;
        
        %Finding pulses in every 200 signals 
        for k = [1:200:length(data) - 200]
            average = sum(data(k:k+199) - bias_level)/200;
            
            %counting pulses
            if average > acceptable_average_max
                pulses_count = pulses_count + 1;
            end
        end
    end
    fclose(file);
end
