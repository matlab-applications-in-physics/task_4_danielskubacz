%MATLAB 2020b
%Name: analyse_peak
%Author: Daniel Skubacz
%Date: 03.01.2021

%File opening:
file_name = 'Co60_1350V_x20_p7_sub';
file = fopen(strcat(file_name,'.dat'));

%Defining maximum size of the part of file
size = 1000000;

%Defining variables:
i = 0;
all_counts = 0;

%Collecting data to the histogram from whole file:
while ~feof(file)
    fseek(file, size*i, 'bof');
    data = fread(file, size, 'uint8');
        
    [counts, compartments] = histcounts(data, 'BinLimits',[0.5, 200.5], 'BinWidth', 1);
    all_counts = all_counts + counts;
    i = i + 1;
end

%Creating histogram:
histogram('BinEdges',compartments,'BinCounts',all_counts);
xlabel('compartment');
ylabel('number of counts');

%Saving histogram to the file:
saveas(gcf,'C:\Users\User\Desktop\Co60_1350V_x20_p7_sub_histogram.pdf', 'pdf');

%Bias value calculeting:
bias_level = bias(compartments, all_counts);

%Counting pulse number:
pulses_count = countPulses('Co60_1350V_x20_p7_sub.dat', all_counts, bias_level);

%Writing results to the file:
results = fopen('C:\Users\User\Desktop\Co60_1350V_x20_p7_sub_peak_analysis_results.dat', 'w');
fprintf(results, 'Co60_1350V_x20_p7_sub\n');
fprintf(results, "bias = %.0f\n", bias_level);
fprintf(results, "pulses_count = %.0f\n", pulses_count);

fclose('all');

        
